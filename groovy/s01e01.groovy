class Todo {
    String titulo
    String descripcion
}

def todos = []
todos[0] = new Todo(titulo: "Lavadora", descripcion: "Poner lavadora")
todos[1] = new Todo(titulo: "Impresora", descripcion: "Comprar cartuchos impresora")
todos[2] = new Todo(titulo: "Peliculas", descripcion: "Devolver peliculas videoclub")

todos.each{todo -> println "${todo.getTitulo()} ${todo.getDescripcion()}"}
return