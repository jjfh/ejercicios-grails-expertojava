class Libro {
    /* Añade aquí la definición de la clase */
    String nombre
    Integer anyo
    String autor
    String editorial
    
    String getAutor() {
        def partes = autor.tokenize(",")
        
        partes[0] = partes[0].trim();
        partes[1] = partes[1].trim();
        
        return partes.get(1) + " " + partes.get(0)
    }
}

/* Crea aquí las tres instancias de libro l1, l2 y l3 */
def l1 = new Libro(nombre: "La colmena", anyo: 1951, autor: "Cela Trulock, Camilo José")
def l2 = new Libro(nombre: "La galatea", anyo: 1585, autor: "Cervantes Saavedra, Miguel")
def l3 = new Libro(nombre: "La dorotea", anyo: 1632, autor: "Lope de Vega y Carpio, Félix Arturo")

assert l1.getNombre() == 'La colmena'
assert l2.getAnyo() == 1585
assert l3.getAutor() == 'Félix Arturo Lope de Vega y Carpio'

/* Añade aquí la asignación de la editorial a todos los libros */
l1.setEditorial("Anaya")
l2.setEditorial("Planeta")
l3.setEditorial("Santillana")

assert l1.getEditorial() == 'Anaya'
assert l2.getEditorial() == 'Planeta'
assert l3.getEditorial() == 'Santillana'