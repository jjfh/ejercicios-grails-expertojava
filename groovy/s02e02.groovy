class Calculadora {
    int op1, op2
    
    static int suma(op1, op2) {
        return op1+op2
    }
    
    static int resta(op1, op2) {
        return op1-op2
    }
    
    static int multiplicacion(op1, op2) {
        return op1*op2
    }
    
    static Float division(op1, op2) {
        try {
            return op1/op2
        } catch (ArithmeticException ex) {
            println "ArithmeticException occured!"
        }
    }
    
    static def mapaOperaciones = ["+":"suma", "-":"resta", "*":"multiplicacion", "/":"division"]

    static main(args) {
        def op1, op2, operador
        
        System.in.withReader {
            print 'Introduce operador1: '
            op1 = it.readLine() as Integer
            print 'Introduce operador2: '
            op2 = it.readLine() as Integer
            print 'Introduce operando: '
            operador = it.readLine()
        }
        
        if (Calculadora.mapaOperaciones.containsKey(operador)){
            def resultado = Calculadora."${mapaOperaciones[operador]}"(op1, op2)
            println "${Calculadora.mapaOperaciones[operador]}($op1, $op2) = $resultado"
        } else {
            println "Operación no permitida"
        }
    }
}