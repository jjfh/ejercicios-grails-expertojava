/* Añade aquí la implementación del factorial en un closure */

//closure que calcula el factorial de un número pasado como parámetro
def factorial = { x ->
    if (x == 0 || x == 1)
        return 1
    else
        return x * call(--x)    
}

assert factorial(1)==1
assert factorial(2)==1*2
assert factorial(3)==1*2*3
assert factorial(4)==1*2*3*4
assert factorial(5)==1*2*3*4*5
assert factorial(6)==1*2*3*4*5*6
assert factorial(7)==1*2*3*4*5*6*7
assert factorial(8)==1*2*3*4*5*6*7*8
assert factorial(9)==1*2*3*4*5*6*7*8*9
assert factorial(10)==1*2*3*4*5*6*7*8*9*10

assert factorial(0)==1

/* Añade a partir de aquí el resto de tareas solicitadas en el ejercicios */

//generamos el factorial de cada uno de los elementos pasado en la lista utilizando el closure creado anteriormente
[1, 2, 3, 4, 5].each {
    println "factorial($it) = ${factorial(it)}"
}

//closure que calcula la fecha correspondiente al día anterior pasado como parámetro
def ayer = { fecha ->
    (--fecha).format('dd/MM/yyyy HH:mm:ss')
}

//closure que calcula la fecha correspondiente al día posterior pasado como parámetro
def manyana = { fecha ->
    (++fecha).format('dd/MM/yyyy HH:mm:ss')
}

//creamos una lista de fechas y utilizamos los closures recién creados para obtener las fechas del día anterior y posterior a todos los elementos de la lista
[new Date(), new Date()-32, new Date()+32].each {
    println "Fecha: $it - Ayer: ${ayer(it)} - Manyana: ${manyana(it)}"
}

return