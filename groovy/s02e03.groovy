/* Añade aquí la implementación solicitada en el ejercicio */

Number.metaClass.moneda = { String locale ->
      def resultado

      switch (locale){
            case 'en_EN': resultado = '£' + delegate
                          break

            case 'en_US': resultado = '\$' + delegate
                          break

            case 'es_ES': resultado = delegate + '€'
                          break

            default: resultado = "Locale no admitido."
      }
      
      return resultado
}

assert 10.2.moneda("en_EN") ==  "£10.2"
assert 10.2.moneda("en_US") ==  "\$10.2"
assert 10.2.moneda("es_ES") ==  "10.2€"

assert 10.moneda("en_EN") ==  "£10"
assert 10.moneda("en_US") ==  "\$10"
assert 10.moneda("es_ES") ==  "10€"

assert new Float(10.2).moneda("en_EN") ==  "£10.2"
assert new Float(10.2).moneda("en_US") ==  "\$10.2"
assert new Float(10.2).moneda("es_ES") ==  "10.2€"

assert new Double(10.2).moneda("en_EN") ==  "£10.2"
assert new Double(10.2).moneda("en_US") ==  "\$10.2"
assert new Double(10.2).moneda("es_ES") ==  "10.2€"