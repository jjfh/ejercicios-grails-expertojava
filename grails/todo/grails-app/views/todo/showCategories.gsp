
<%@ page import="es.ua.expertojava.todo.Todo" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'todo.label', default: 'Todo')}" />
		<title>List by category</title>
	</head>
	<body>
		<a href="#show-todo" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-todo" class="content scaffold-show" role="main">
			<h1>Filtrar por categorías</h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>

			<div>
				<g:form action="listTodosByCategory" >
					<fieldset class="form">
						<g:each in="${categoryInstanceList}" var="categoryInstance">
							<g:checkBox name="category" value="${categoryInstance?.id}"/> ${categoryInstance?.name}
						</g:each>
					</fieldset>
					<fieldset class="buttons">
						<g:submitButton name="List" class="save" value="Listar Todos"/>
					</fieldset>
				</g:form>
			</div>
		</div>
	</body>
</html>
