<div id="header">
    <div id="menu">
        <nobr>
            <sec:ifLoggedIn>
                Bienvenido&nbsp;<b><sec:username/>&nbsp;</b> |
                <form name="logout" method="POST" action="${createLink(controller:'logout') }">
                    <input type="submit" value="logout"></form>
            </sec:ifLoggedIn>
            <sec:ifNotLoggedIn>
                <g:link controller="login" action="auth">Login</g:link>
            </sec:ifNotLoggedIn>
        </nobr>
    </div>
</div>