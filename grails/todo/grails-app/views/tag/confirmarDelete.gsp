<%@ page import="es.ua.expertojava.todo.Tag" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'tag.label', default: 'Tag')}" />
    <title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>
<div id="show-tag" class="content scaffold-show" role="main">
    <!--<h1>Eliminar Tag</h1>
    <img src="${resource(dir: 'images', file: 'warning.png')}" alt="Warning"/>
    <h3>¿Está seguro que desea eliminar el tag "${tagInstance?.name}"?</h3>-->

    <h3>¿Está seguro que desea eliminar el tag "${tagInstance?.name}"?</h3>
    <asset:image src="warning.png" style="width: 100px"/>

    <g:form url="[resource:tagInstance, action:'delete']" method="DELETE">
        <fieldset class="buttons">
            <g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" />
        </fieldset>
    </g:form>
</div>
</body>
</html>