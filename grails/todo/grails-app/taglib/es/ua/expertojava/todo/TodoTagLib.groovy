package es.ua.expertojava.todo

class TodoTagLib {
    static defaultEncodeAs = [taglib:'none']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]

    //espacio de nombres para nuestras etiquetas
    static namespace = 'todo'

    //etiqueta que nos permite, a partir de un valor booleano, imprimir un icono u otro
    // Ejemplo: <todo:printIconFromBoolean value=${todoInstance.done}/>

    def printIconFromBoolean = { attrs ->
        def value = attrs['value']
        if(value) {
            out << asset.image(src:"completed.png")
        }
        else {
            out << asset.image(src:"uncompleted.png")
        }
    }
}
