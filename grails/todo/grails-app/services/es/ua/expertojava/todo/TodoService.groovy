package es.ua.expertojava.todo

class TodoService {

    def getNextTodos(Integer days, params) {
        Date now = new Date(System.currentTimeMillis())
        Date to = now + days
        Todo.findAllByDateBetween(now, to, params)
    }

    def countNextTodos(Integer days) {
        Date now = new Date(System.currentTimeMillis())
        Date to = now + days
        Todo.countByDateBetween(now, to)
    }

    //ejercicio 5.6.2
    def deleteTodo(Todo todoInstance) {
        def tags = todoInstance?.tags?.findAll()

        tags.each {
            todoInstance.removeFromTags(it)
        }

        todoInstance.delete()
    }

    //ejercicio 5.6.3
    // función inicial, antes de introducir el control de usuarios
    /*def getTodosByCategoryOrderByDate(List<Category> categorias) {
        Todo.findAllByCategoryInList(categorias, [sort:"date",order:"desc"])
    }*/

    // mejora al introducir el control de usuarios, ya que además de ordenar por la fecha, sólo muestra las tareas asociadas al usuario
    def getTodosByCategoryAndUserOrderByDate(List<Category> categorias, User usuario) {
        Todo.findAllByUserAndCategoryInList(usuario,categorias,[sort:"date",order:"desc"])
    }

    def countTodosByCategory(List<Category> categorias) {
        Todo.countByCategoryInList(categorias)
    }

    //ejercicio 5.6.4
    def saveTodo(Todo todoInstance) {
        if (todoInstance.completed && !todoInstance.dateDone) {
            todoInstance.dateDone = new Date()
        }

        todoInstance.save(flush: true)
    }

    //devuelva las últimas tareas realizadas desde el momento actual hasta hace 'hours' horas.
    def lastTodosDone(Integer hours) {
        Date endDate = new Date(System.currentTimeMillis())
        //a la fecha actual le resto 'hours' horas (pasadas a milisegundos
        Date startDate = new Date(System.currentTimeMillis() - hours*60*60*1000)

        Todo.findAllByDateDoneBetween(endDate, startDate)
    }
}
