package es.ua.expertojava.todo

class CategoryService {

    //ejercicio 5.6.2
    def deleteCategory(Category categoryInstance) {
        Todo.findAllByCategory(categoryInstance)?.each { c ->
            c.category = null
            c.save()
        }

        categoryInstance.delete()
    }

}
