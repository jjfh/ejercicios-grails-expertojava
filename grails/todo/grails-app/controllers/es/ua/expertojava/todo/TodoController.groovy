package es.ua.expertojava.todo



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TodoController {

    def todoService
    def springSecurityService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)

        User user = springSecurityService.isLoggedIn() ? springSecurityService.getCurrentUser() : null

        if(user.getUsername()=="admin") {
            respond Todo.list(params), model:[todoInstanceCount: Todo.count()]
        }
        else {
            def listaAux = []
            listaAux = Todo.findAllByUser(user)
            if (listaAux == null) listaAux = []

            respond listaAux,
                    model:[todoInstanceCount: listaAux.size()],
                    view: "index"
        }
    }

    def show(Todo todoInstance) {
        respond todoInstance
    }

    def create() {
        respond new Todo(params)
    }

    @Transactional
    def save(Todo todoInstance) {
        if (todoInstance != null) {
            User user = springSecurityService.isLoggedIn() ? springSecurityService.getCurrentUser() : null

            user.addToTodos(todoInstance)
            todoInstance.user= new User()
            todoInstance.user= user
        }

        if (todoInstance == null) {
            notFound()
            return
        }

        if (todoInstance.hasErrors()) {
            respond todoInstance.errors, view:'create'
            return
        }

        //todoInstance.save flush:true
        todoService.saveTodo(todoInstance)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'todo.label', default: 'Todo'), todoInstance.id])
                redirect todoInstance
            }
            '*' { respond todoInstance, [status: CREATED] }
        }
    }

    def edit(Todo todoInstance) {
        respond todoInstance
    }

    @Transactional
    def update(Todo todoInstance) {
        if (todoInstance == null) {
            notFound()
            return
        }

        if (todoInstance.hasErrors()) {
            respond todoInstance.errors, view:'edit'
            return
        }

        //todoInstance.save flush:true
        todoService.saveTodo(todoInstance)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Todo.label', default: 'Todo'), todoInstance.id])
                redirect todoInstance
            }
            '*'{ respond todoInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Todo todoInstance) {

        if (todoInstance == null) {
            notFound()
            return
        }

        //todoInstance.delete flush:true
        todoService.deleteTodo(todoInstance)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Todo.label', default: 'Todo'), todoInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'todo.label', default: 'Todo'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def listNextTodos(Integer days) {
        respond todoService.getNextTodos(days, params),
                model: [todoInstanceCount: todoService.countNextTodos(days)],
                view: "index"
    }

    def listTodosByCategory() {
        def categorias = []
        User user = springSecurityService.isLoggedIn() ? springSecurityService.getCurrentUser() : null

        params?.category?.each { id ->
            categorias.add(Category.get(id))
        }

        respond todoService.getTodosByCategoryAndUserOrderByDate(categorias,user),
                model: [todoInstanceCount: todoService.countTodosByCategory(categorias)],
                view: "listByCategory"
    }

    def showCategories() {
        render model: [categoryInstanceList: Category.list()],
                view: "showCategories"
    }

    def showTodosByUser() {
        def usuario = User.findByUsername(params.username)
        respond Todo.findAllByUser(usuario),
                view: "showTodos"
    }
}
