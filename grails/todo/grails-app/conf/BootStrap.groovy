import es.ua.expertojava.todo.*

class BootStrap {

    def init = { servletContext ->
        try {
            //creamos los roles
            Role roleAdmin = new Role(authority: "ROLE_ADMIN").save();
            Role roleBasic = new Role(authority: "ROLE_BASIC").save();

            //creamos los usuarios
            User admin = new User(username: "admin", password: "admin", confirmPassword: "admin", name: "Admin", surnames: "Admin", email: "admin@ua.es").save();
            User usuario1 = new User(username: "usuario1", password: "usuario1", confirmPassword: "usuario1", name: "Usuario1", surnames: "Usuario1", email: "usuario1@ua.es").save();
            User usuario2 = new User(username: "usuario2", password: "usuario2", confirmPassword: "usuario2", name: "Usuario2", surnames: "Usuario2", email: "usuario2@ua.es").save();

            //asignamos un role a los diferentes users
            PersonRole.create(admin, roleAdmin).save();
            PersonRole.create(usuario1, roleBasic).save();
            PersonRole.create(usuario2, roleBasic).save();

            def categoryHome = new Category(name: "Hogar").save()
            def categoryJob = new Category(name: "Trabajo").save()

            def tagEasy = new Tag(name: "Fácil", color: "#ff0000").save()
            def tagDifficult = new Tag(name: "Difícil", color: "#00ff00").save()
            def tagArt = new Tag(name: "Arte", color: "#0000ff").save()
            def tagRoutine = new Tag(name: "Rutina", color: "#ffff00").save()
            def tagKitchen = new Tag(name: "Cocina", color: "#D0D0D0").save()

            def todoPaintKitchen = new Todo(title: "Pintar cocina", completed: true, date: new Date() + 1)
            def todoCollectPost = new Todo(title: "Recoger correo postal", completed: false, date: new Date() + 2)
            def todoBakeCake = new Todo(title: "Cocinar pastel", completed: true, date: new Date() + 4)
            def todoWriteUnitTests = new Todo(title: "Escribir tests unitarios", completed: false, date: new Date())

            usuario1.addToTodos(todoPaintKitchen)
            usuario1.addToTodos(todoCollectPost)
            usuario2.addToTodos(todoBakeCake)
            usuario2.addToTodos(todoWriteUnitTests)

            todoPaintKitchen.addToTags(tagDifficult)
            todoPaintKitchen.addToTags(tagArt)
            todoPaintKitchen.addToTags(tagKitchen)
            todoPaintKitchen.category = categoryHome
            todoPaintKitchen.save()

            todoCollectPost.addToTags(tagRoutine)
            todoCollectPost.category = categoryJob
            todoCollectPost.save()

            todoBakeCake.addToTags(tagEasy)
            todoBakeCake.addToTags(tagKitchen)
            todoBakeCake.category = categoryHome
            todoBakeCake.save()

            todoWriteUnitTests.addToTags(tagEasy)
            todoWriteUnitTests.category = categoryJob
            todoWriteUnitTests.save()
        } catch (Exception ex) {
            println ex
        }
    }
    def destroy = { }
}
