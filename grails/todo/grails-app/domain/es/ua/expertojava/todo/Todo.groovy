package es.ua.expertojava.todo

class Todo {
    String title
    Boolean completed = false
    Date date
    String url
    Category category
    Date reminderDate
    String description
    User user

    Date dateCreated
    Date lastUpdated

    Date dateDone

    static hasMany = [tags:Tag]
    static belongsTo = [Tag]

    static constraints = {
        title(blank:false)
        completed(nullable:false)
        date(nullable:false)
        url(nullable:true, url:true)
        category(nullable:true)
        reminderDate(nullable:true,
            validator: {val, obj ->
                if (val && obj?.date) {
                    return val.before(obj.date)
                }
                return true
            })
        description(blank:true, nullable:true, maxSize:1000)

        dateDone(nullable:true)
    }

    String toString(){
        title
    }
}