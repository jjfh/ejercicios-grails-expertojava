package es.ua.expertojava.todo

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(TodoService)
class TodoServiceSpec extends Specification {

    def usuario1, usuario2

    def setup() {
        usuario1 = new User(username: "usuario1", password: "usuario1", confirmPassword: "usuario1", name: "Usuario1", surnames: "Usuario1", email: "usuario1@ua.es");
        usuario2 = new User(username: "usuario2", password: "usuario2", confirmPassword: "usuario2", name: "Usuario2", surnames: "Usuario2", email: "usuario2@ua.es");
    }

    def cleanup() {
    }

    // tests ejercicio 6.4.3
    def "El método countNextNodos devuelve el número de tareas que deben realizarse en los próximos días (valor pasado como parámetro)"() {
        given:
            def todoPaintKitchen = new Todo(title: "Pintar cocina", completed: true, date: new Date() + 1, user: usuario1)
            def todoCollectPost = new Todo(title: "Recoger correo postal", completed: false, date: new Date() + 2, user: usuario1)
            def todoBakeCake = new Todo(title: "Cocinar pastel", completed: true, date: new Date() + 4, user: usuario2)
            def todoWriteUnitTests = new Todo(title: "Escribir tests unitarios", completed: false, date: new Date(), user: usuario2)
        and:
            mockDomain(Todo,[todoPaintKitchen,todoCollectPost,todoBakeCake,todoWriteUnitTests])
        when:
            def countNodos = service.countNextTodos(2)
        then:
            Todo.count() == 4
        and:
            countNodos == 2
    }

    void "El método saveTodo(), almacena la fecha de realización de la misma si la tarea ha sido completada"() {
        given:
            def todoWriteUnitTests = new Todo(title: "Escribir tests unitarios", completed: false, date: new Date(), user: usuario2)
        expect:
            todoWriteUnitTests.completed == false
            todoWriteUnitTests.dateDone == null
        when:
            todoWriteUnitTests.completed = true
        and:
            service.saveTodo(todoWriteUnitTests)
        then:
            todoWriteUnitTests.dateDone != null
        and:
            todoWriteUnitTests.dateDone instanceof Date
    }

    void "El método saveTodo(), no actualiza la fecha de realización de la misma si la tarea no ha sido completada"() {
        given:
            def todoWriteUnitTests = new Todo(title: "Escribir tests unitarios", completed: false, date: new Date(), user: usuario2)
        expect:
            todoWriteUnitTests.completed == false
            todoWriteUnitTests.dateDone == null
        when:
            todoWriteUnitTests.completed = false
        and:
            service.saveTodo(todoWriteUnitTests)
        then:
            todoWriteUnitTests.dateDone == null
    }
}
