package es.ua.expertojava.todo

import grails.test.mixin.TestFor
import spock.lang.Specification
import spock.lang.Unroll

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Todo)
class TodoSpec extends Specification {

    def usuario1

    def setup() {
        usuario1 = new User(username: "usuario1", password: "usuario1", confirmPassword: "usuario1", name: "Usuario1", surnames: "Usuario1", email: "usuario1@ua.es").save();
    }

    def cleanup() {
    }

    //ejercicio 6.4.1
    @Unroll
    def "La fecha de recordatorio nunca pueda ser posterior a la fecha en la que debe realizarse la tarea"() {
        given:
            def t1 = new Todo(title: "Pintar cocina", completed: true, date: new Date(), reminderDate: new Date() + 1, user: usuario1)
        when:
            t1.validate()
        then:
            t1?.errors['reminderDate']
    }

    @Unroll
    def "Si la fecha de recordatorio es anterior a la fecha en la que debe realizarse la tarea, este campo no dará problemas"() {
        given:
            def t1 = new Todo(title: "Pintar cocina", completed: true, date: new Date(), reminderDate: new Date() - 1, user: usuario1)
        when:
            t1.validate()
        then:
            !t1?.errors['reminderDate']
    }

}
